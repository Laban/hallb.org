﻿#include <iostream>
#include <string>
#include <math.h>

#define NR_OF_WORDS_AND_SPACES 123

char characters[] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

using namespace std;

void convertFromHexToBin(string str, string *&arr) { 
	// Convert from hexadecimal to binary. One ASCII character. 
   	// is represented by two character from the parameter str.     
   	// Separate each converted character, that is each byte (8 bits) 
   	// should be added to the array arr.
}

void convertFromBinToInt(string *&arr) {
	// Convert from binary to integer and add it to the array arr. 
}

void convertFromIntToChar(string *&arr) {
	// Convert from integer to char and add it to the array arr.
}

void printArray(string *arr) {
	 // Prints all elements in the arr as a complete string to stdout
	for (int i = 0; i < NR_OF_WORDS_AND_SPACES; i++)
		cout << arr[i];

	cout << endl << endl;
}

int main() {
	//_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // Rapportera minnesläckor

	string str = "4e69206861722076617269742065787472656d742064756b74696761206f6368206c276f2773742064656e6e612075707067696674206d656420657863656c6c656e7320212053746f7274207461636b2066276f2772206572206d65647665726b616e202e202f20496d6d616e75656c6c2026204a6573706572";
	string *arr = new string[NR_OF_WORDS_AND_SPACES];

	convertFromHexToBin(str, arr);
	printArray(arr);

	convertFromBinToInt(arr);
	printArray(arr);

	convertFromIntToChar(arr);
	printArray(arr);

	delete []arr;
	return 0;
}