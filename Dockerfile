FROM resin/armhf-alpine-node:slim
ADD . /code
WORKDIR /code
CMD ["npm", "start"]
